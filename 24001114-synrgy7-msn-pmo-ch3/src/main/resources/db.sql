create table users
(
    id            serial not null,
    username      varchar(255),
    email_address varchar(255),
    password      varchar(255),
    constraint pk_users primary key (id)
);

create table merchants
(
    id                serial not null,
    merchant_name     varchar(255),
    merchant_location varchar(500),
    is_open           bool,
    constraint pk_merchant primary key (id)
);

create table orders
(
    id                  serial    not null,
    order_time          timestamp not null default current_timestamp,
    destination_address varchar(500),
    user_id             int       not null,
    completed           bool,
    constraint pk_orders primary key (id),
    constraint fk_users foreign key (user_id) references users (id)
);

create table products
(
    id           serial not null,
    product_name varchar(255),
    price        int    not null,
    merchant_id  int    not null,
    constraint pk_product primary key (id),
    constraint fk_merchant foreign key (merchant_id) references merchants (id)
);

create table order_detail
(
    id          serial not null,
    order_id    int    not null,
    product_id  int    not null,
    quantity    int    not null,
    total_price int    not null,
    constraint pk_order_detail primary key (id),
    constraint fk_order foreign key (order_id) references orders (id),
    constraint fk_product foreign key (product_id) references products (id)
);

insert into users(username, email_address, "password")
values ('abilsabili', 'abilsabili50@gmail.com', 'admin123'),
       ('abilsabili123', 'abilsabili123@gmail.com', 'abil123'),
       ('fahrul123', 'fahrul123@gmail.com', 'fahrul123'),
       ('syaugi123', 'syaugi123@gmail.com', 'syaugi123'),
       ('putra123', 'putra123@gmail.com', 'putra123');

insert into merchants(merchant_name, merchant_location, is_open)
values ('toko buku abadi', 'surabaya', true),
       ('toko alat musik', 'banten', true),
       ('toko jaya makmur', 'surabaya', false),
       ('toko barokah', 'bandung', true);

insert into products(product_name, price, merchant_id)
values ('Dilan 1990', 60000, 1),
       ('Buku Pantun', 60000, 1),
       ('Gitar', 300000, 2),
       ('Beras 1Kg', 24000, 3);

insert into orders(destination_address, user_id, completed)
values ('Surabaya, Jawa Timur', 2, false),
       ('Bojonegoro, Jawa Timur', 2, true),
       ('Bandung, Jawa Barat', 5, false),
       ('Jakarta, DKI Jakarta', 4, false);

insert into order_detail(order_id, product_id, quantity, total_price)
values (1, 3, 1, 300000),
       (2, 1, 2, 120000),
       (3, 2, 1, 60000),
       (3, 3, 1, 300000),
       (4, 3, 2, 600000),
       (4, 1, 1, 60000),
       (4, 2, 1, 60000);

update merchants
set merchant_name = 'toko peralatan musik'
where id = 2;

delete
from merchants
where id = 4;

-- untuk melihat seluruh data pada tiap tabel
select * from users;
select * from merchants;
select * from products;
select * from orders where completed = false;
select * from order_detail;

-- untuk mengecek seluruh order yg belum selesai
select u.username          as username,
       sum(od.quantity)    as total_qty,
       sum(od.total_price) as total_price
from users u
         inner join orders o on o.user_id = u.id
         inner join order_detail od on od.order_id = o.id
         inner join products p on p.id = od.product_id
         inner join merchants m on m.id = p.merchant_id
where o.completed = false
group by u.username;

-- untuk mengecek total order yg dilakukan oleh user
select u.username, count(*) as total_order from users u
         right join orders o on o.user_id = u.id
group by u.username;

-- untuk mengecek total biaya yg dikeluarkan oleh user
select u.username, sum(od.total_price) as total_cost
from users u
         inner join orders o on o.user_id = u.id
         inner join order_detail od on od.order_id = o.id
         inner join products p on p.id = od.product_id
         inner join merchants m on m.id = p.merchant_id
group by u.id
order by total_cost desc;


-- drop table users, merchants, order_detail, orders, products;