package Entities;

import lombok.Getter;

import java.util.HashMap;

@Getter
public class Order{
    private final HashMap<Integer, Integer> orderDetail; // <menuId, qty>

    public Order(){
        this.orderDetail = new HashMap<>();
    }

    public int getQtyByMenuId(int menuId){
        return (this.orderDetail.get(menuId) == null) ? 0 : this.orderDetail.get(menuId);
    }

    public void addOrder(int menuId, int qty){
        this.orderDetail.put(menuId, qty);
    }
}