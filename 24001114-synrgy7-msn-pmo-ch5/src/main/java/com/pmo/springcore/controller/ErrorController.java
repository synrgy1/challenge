package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
@Slf4j
public class ErrorController {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Response<Object>> constraintViolationException(ConstraintViolationException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Response.<Object>builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Response<Object>> responseStatusException(ResponseStatusException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(exception.getStatusCode())
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Response<Object>> jasperReportException(JRException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public ResponseEntity<Response<Object>> mediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(exception.getStatusCode())
                .body(Response.builder().errors("media type not supported").status("ERROR").build());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Response<Object>> runtimeException(RuntimeException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Response<Object>> exception(Exception exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

}
