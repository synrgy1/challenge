package com.pmo.springcore.service;

import com.pmo.springcore.dto.product.*;
import com.pmo.springcore.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IProductService {
    ProductResponse create(User user, CreateProductRequest request);

    ProductResponse getById(String productId);

    Page<ProductResponse> listProducts(Pageable pageable);

    ProductResponse update(User user, UpdateProductRequest request, String productId);

    void delete(User user, String productId);
}
