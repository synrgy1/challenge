package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.user.LoginRequest;
import com.pmo.springcore.dto.user.TokenResponse;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IUserRepository;
import com.pmo.springcore.service.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
public class AuthService implements IAuthService {

    private final IUserRepository userRepository;

    private final ValidationService validator;

    @Autowired
    public AuthService(IUserRepository userRepository, ValidationService validator){
        this.userRepository = userRepository;
        this.validator = validator;
    }

    @Transactional
    @Override
    public TokenResponse login(LoginRequest request){
        validator.validate(request);

        User user = userRepository.findFirstByEmailAddress(request.getEmail())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "email or password wrong"));

        if(!BCrypt.checkpw(request.getPassword(), user.getPassword())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "email or password wrong");
        }

        user.setToken(UUID.randomUUID().toString());
        user.generateExpiredToken();
        userRepository.save(user);

        return TokenResponse.builder().token(user.getToken()).expiredAt(user.getTokenExpiredAt()).build();
    }
}
