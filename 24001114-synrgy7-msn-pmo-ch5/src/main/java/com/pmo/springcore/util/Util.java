package com.pmo.springcore.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

public class Util {
    public static UUID convertStringIntoUUID(String id){
        try {
            return UUID.fromString(id);
        }catch (IllegalArgumentException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid id");
        }
    }
}
