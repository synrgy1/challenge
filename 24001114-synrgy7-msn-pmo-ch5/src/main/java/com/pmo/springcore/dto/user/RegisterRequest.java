package com.pmo.springcore.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterRequest {
    @NotBlank(message = "username can't be empty")
    private String username;

    @NotBlank(message = "email can't be empty")
    @Email(message = "email must be filled in with the correct format")
    private String email;

    @NotBlank(message = "password can't be empty")
    @Size(min = 6, message = "password minimum characters is 6")
    private String password;

    private Integer roleId;
}
