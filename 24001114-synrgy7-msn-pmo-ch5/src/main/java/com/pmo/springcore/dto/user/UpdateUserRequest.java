package com.pmo.springcore.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateUserRequest {

    private String username;

    @Email(message = "email must be filled in with the correct format")
    private String email;

    @Size(min = 6, message = "password minimum characters is 6")
    private String password;
}
