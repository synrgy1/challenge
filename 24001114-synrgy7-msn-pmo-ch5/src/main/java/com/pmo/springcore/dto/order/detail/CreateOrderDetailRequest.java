package com.pmo.springcore.dto.order.detail;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateOrderDetailRequest {
    @NotNull(message = "quantity can't be empty")
    private Long quantity;

    @NotBlank(message = "order id can't be empty")
    private String orderId;

    @NotBlank(message = "product id can't be empty")
    private String productId;
}
