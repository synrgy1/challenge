package com.pmo.springcore.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.user.LoginRequest;
import com.pmo.springcore.dto.user.TokenResponse;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IUserRepository;
import org.antlr.v4.runtime.Token;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AuthControllerTest {

    private final MockMvc mockMvc;

    private final IUserRepository userRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    public AuthControllerTest(MockMvc mockMvc, IUserRepository userRepository, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.userRepository = userRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void testObjectMapper() throws Exception {
        TestRestTemplate restTemplate = new TestRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set("Content-Type", "application/json");

        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        userRepository.save(user);

//        LoginRequest req = new LoginRequest();
//        req.setEmail("test@test.test");
//        req.setPassword("test123");

        JSONObject userReq = new JSONObject();
        userReq.put("email", "test@test.test");
        userReq.put("password", "test123");

        HttpEntity<String> req = new HttpEntity<>(userReq.toString(), headers);

        String res = restTemplate.postForObject("http://localhost:8080/api/v1/auth/login", req, String.class);
        Response<TokenResponse> tokenResponse = objectMapper.readValue(res, new TypeReference<Response<TokenResponse>>() {
        });

        assertNotNull(res);
        assertNotNull(tokenResponse);
        assertNotNull(tokenResponse.getStatus());
        assertNotNull(tokenResponse.getData());
        assertNotNull(tokenResponse.getMessage());
        System.out.println(res);
        System.out.println(tokenResponse);

    }

    @Test
    void testLoginSuccess() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        userRepository.save(user);

        LoginRequest req = new LoginRequest();
        req.setEmail("test@test.test");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<TokenResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNotNull(res.getData().getToken());
            assertNotNull(res.getData().getExpiredAt());

            User userDb = userRepository.findFirstByEmailAddress("test@test.test").orElse(null);
            assertNotNull(userDb);
            assertEquals(userDb.getToken(), res.getData().getToken());
            assertEquals(userDb.getTokenExpiredAt(), res.getData().getExpiredAt());
        });
    }

    @Test
    void testLoginWrongPassword() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        userRepository.save(user);

        LoginRequest req = new LoginRequest();
        req.setEmail("test@test.test");
        req.setPassword("salah123");

        mockMvc.perform(
                post("/api/v1/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<TokenResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testLoginEmailUnregistered() throws Exception {
        LoginRequest req = new LoginRequest();
        req.setEmail("test@test.test");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<TokenResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }
}