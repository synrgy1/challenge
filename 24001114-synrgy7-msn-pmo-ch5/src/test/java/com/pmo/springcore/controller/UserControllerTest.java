package com.pmo.springcore.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmo.springcore.dto.user.RegisterRequest;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.user.UpdateUserRequest;
import com.pmo.springcore.dto.user.UserResponse;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    private final MockMvc mockMvc;

    private final IUserRepository userRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    UserControllerTest(MockMvc mockMvc, IUserRepository userRepository, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.userRepository = userRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void testRegisterSuccess() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("Test");
        req.setEmail("test@test.test");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("OK", res.getStatus());
            assertNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithBlankRequest() throws Exception{
        RegisterRequest req = new RegisterRequest();

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithBlankUsername() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("");
        req.setEmail("test@test.test");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithBlankEmail() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("test");
        req.setEmail("");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithInvalidEmail() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("test");
        req.setEmail("testtesttest");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithBlankPassword() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("test");
        req.setEmail("test@test.test");
        req.setPassword("");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithUnderSixCharacterPassword() throws Exception{
        RegisterRequest req = new RegisterRequest();
        req.setUsername("test");
        req.setEmail("test@test.test");
        req.setPassword("test1");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testRegisterWithDuplicateEmail() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword("test123");
        userRepository.save(user);

        RegisterRequest req = new RegisterRequest();
        req.setUsername("test");
        req.setEmail("test@test.test");
        req.setPassword("test123");

        mockMvc.perform(
                post("/api/v1/users/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testGetUserUnauthorized() throws Exception {
        mockMvc.perform(
                get("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "salah-token")
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testGetUserWithoutToken() throws Exception {
        mockMvc.perform(
                get("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
        });
    }

    @Test
    void testGetUserWithExpiredToken() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setToken("test");
        user.setTokenExpiredAt(System.currentTimeMillis() - 1000L);
        userRepository.save(user);

        mockMvc.perform(
                get("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<String> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertEquals("ERROR", res.getStatus());
            assertNotNull(res.getErrors());
            assertNull(res.getData());
        });
    }

    @Test
    void testGetUserSuccess() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        mockMvc.perform(
                get("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<UserResponse> res =objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNotNull(res.getData());
            assertEquals("OK", res.getStatus());
            assertEquals("test", res.getData().getUsername());
            assertEquals("test@test.test", res.getData().getEmail());
        });
    }

    @Test
    void testUpdateUserUnauthorized() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        UpdateUserRequest req = new UpdateUserRequest();
        req.setEmail("test@test.com");
        req.setUsername("test123");
        req.setPassword("testtest123");

        mockMvc.perform(
                patch("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "salah")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<UserResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testUpdateUserSuccess() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        UpdateUserRequest req = new UpdateUserRequest();
        req.setEmail("test@test.com");
        req.setUsername("test123");
        req.setPassword("testtest123");

        mockMvc.perform(
                patch("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<UserResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(userDB);
            assertEquals(userDB.getEmailAddress(), req.getEmail());
            assertEquals(userDB.getUsername(), req.getUsername());
            assertTrue(BCrypt.checkpw(req.getPassword(), userDB.getPassword()));
        });
    }

    @Test
    void testUpdateUserSuccessEmailOnly() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        UpdateUserRequest req = new UpdateUserRequest();
        req.setEmail("test@test.com");

        mockMvc.perform(
                patch("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<UserResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(userDB);
            assertEquals(userDB.getEmailAddress(), req.getEmail());
        });
    }

    @Test
    void testUpdateUserSuccessUsernameOnly() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        UpdateUserRequest req = new UpdateUserRequest();
        req.setUsername("test123");

        mockMvc.perform(
                patch("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<UserResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(userDB);
            assertEquals(userDB.getUsername(), req.getUsername());
        });
    }

    @Test
    void testUpdateUserSuccessPasswordOnly() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setPassword(BCrypt.hashpw("test123", BCrypt.gensalt()));
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        UpdateUserRequest req = new UpdateUserRequest();
        req.setPassword("testtest123");

        mockMvc.perform(
                patch("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<UserResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(userDB);
            assertTrue(BCrypt.checkpw(req.getPassword(), userDB.getPassword()));
        });
    }

    @Test
    void testDeleteUserUnauthorized() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        mockMvc.perform(
                delete("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "salah_token")
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());

            // to check that the data still exists
            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(userDB);

        });
    }

    @Test
    void testDeleteUserSuccess() throws Exception{
        User user = new User();
        user.setUsername("test");
        user.setEmailAddress("test@test.test");
        user.setToken("test");
        user.generateExpiredToken();
        userRepository.save(user);

        mockMvc.perform(
                delete("/api/v1/users/current")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("OK", res.getStatus());

            // to check that the data still exists
            User userDB = userRepository.findFirstByToken("test").orElse(null);
            assertNull(userDB);

        });
    }
}