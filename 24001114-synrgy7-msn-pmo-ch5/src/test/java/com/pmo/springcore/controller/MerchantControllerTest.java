package com.pmo.springcore.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.merchant.CreateMerchantRequest;
import com.pmo.springcore.dto.merchant.MerchantResponse;
import com.pmo.springcore.dto.merchant.UpdateMerchantRequest;
import com.pmo.springcore.entity.Merchant;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IMerchantRepository;
import com.pmo.springcore.repository.IUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class MerchantControllerTest {

    private final MockMvc mockMvc;
    private final IUserRepository userRepository;
    private final IMerchantRepository merchantRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    public MerchantControllerTest(MockMvc mockMvc, IUserRepository userRepository, IMerchantRepository merchantRepository, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.userRepository = userRepository;
        this.merchantRepository = merchantRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setUp() {
        merchantRepository.deleteAll();
        userRepository.deleteAll();

        User user = new User();
        user.setToken("test");
        user.setRoleId(2);
        user.generateExpiredToken();

        userRepository.save(user);
    }

    @AfterEach
    void tearDown() {
        merchantRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void testCreateMerchantUnauthorized() throws Exception {
        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantName("Test Merchant");
        req.setMerchantLocation("Test Location");
        req.setIsOpen(true);

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "salah token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testCreateMerchantWithBuyerRole() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);
        user.setRoleId(1);
        userRepository.save(user);

        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantName("Test Merchant");
        req.setMerchantLocation("Test Location");
        req.setIsOpen(true);

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testCreateMerchantErrorWithBlankIsOpenField() throws Exception {

        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantName("Test Merchant");
        req.setMerchantLocation("Test Location");

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testCreateMerchantErrorWithBlankLocationField() throws Exception {

        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantName("Test Merchant");
        req.setIsOpen(true);

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testCreateMerchantErrorWithBlankNameField() throws Exception {

        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantLocation("Test Location");
        req.setIsOpen(true);

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testCreateMerchantSuccess() throws Exception {

        CreateMerchantRequest req = new CreateMerchantRequest();
        req.setMerchantName("Test Merchant");
        req.setMerchantLocation("Test Location");
        req.setIsOpen(true);

        mockMvc.perform(
                post("/api/v1/merchants/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNotNull(res.getData());
            assertEquals("OK", res.getStatus());

            Merchant merchant = merchantRepository.findById(res.getData().getId()).orElse(null);
            assertNotNull(merchant);
            assertEquals(merchant.getMerchantName(), req.getMerchantName());
            assertEquals(merchant.getMerchantLocation(), req.getMerchantLocation());

            User user = userRepository.findFirstByToken("test").orElse(null);
            assertNotNull(user);

            assertEquals(user.getId(), merchant.getUser().getId());
        });
    }

    @Test
    void testGetMerchantByIDSuccess() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Test Name");
        merchant.setMerchantLocation("Test Location");
        merchant.setOpen(true);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                get("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNotNull(res.getData());
            assertEquals("OK", res.getStatus());
            assertEquals("Test Name", res.getData().getMerchantName());
            assertEquals("Test Location", res.getData().getMerchantLocation());
            assertTrue(res.getData().getIsOpen());
        });

    }

    @Test
    void testGetMerchantByIDNotFound() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Test Name");
        merchant.setMerchantLocation("Test Location");
        merchant.setOpen(true);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                get("/api/v1/merchants/" + UUID.randomUUID())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });

    }

    @Test
    void testGetMerchantByIDWithInvalidMerchantID() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Test Name");
        merchant.setMerchantLocation("Test Location");
        merchant.setOpen(true);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                get("/api/v1/merchants/salah-" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("ERROR", res.getStatus());
        });
    }

    @Test
    void testUpdateMerchantByDifferentOwner() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        User user2 = new User();
        user2.setToken("beda-token");
        user2.setRoleId(2);
        user2.generateExpiredToken();
        userRepository.save(user2);

        UpdateMerchantRequest req = new UpdateMerchantRequest();
        req.setIsOpen(true);

        mockMvc.perform(
                patch("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateMerchantByBuyerRole() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);
        user.setRoleId(1);
        userRepository.save(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        UpdateMerchantRequest req = new UpdateMerchantRequest();
        req.setIsOpen(true);

        mockMvc.perform(
                patch("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateMerchantUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);

        merchantRepository.save(merchant);

        UpdateMerchantRequest req = new UpdateMerchantRequest();
        req.setIsOpen(true);

        mockMvc.perform(
                patch("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateMerchantSuccess() throws Exception {
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);

        merchantRepository.save(merchant);

        UpdateMerchantRequest req = new UpdateMerchantRequest();
        req.setIsOpen(true);

        mockMvc.perform(
                patch("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            Merchant merchantDB = merchantRepository.findById(res.getData().getId()).orElse(null);
            assertNotNull(merchantDB);
            assertTrue(merchantDB.isOpen());

        });
    }

    @Test
    void testDeleteMerchantByDifferentOwner() throws Exception{
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        User user2 = new User();
        user2.setToken("beda-token");
        user2.setRoleId(2);
        user2.generateExpiredToken();
        userRepository.save(user2);

        mockMvc.perform(
                delete("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteMerchantByBuyerRole() throws Exception{
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);
        user.setRoleId(1);
        userRepository.save(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                delete("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteMerchantUnauthorized() throws Exception{
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        User user2 = new User();
        user2.setToken("beda-token");
        user2.setRoleId(2);
        user2.generateExpiredToken();
        userRepository.save(user2);

        mockMvc.perform(
                delete("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteMerchantNotFound() throws Exception{
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                delete("/api/v1/merchants/" + UUID.randomUUID())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteMerchantSuccess() throws Exception{
        User user = userRepository.findFirstByToken("test").orElse(null);
        assertNotNull(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant Test");
        merchant.setMerchantLocation("Lokasi Test");
        merchant.setOpen(false);
        merchant.setUser(user);
        merchantRepository.save(merchant);

        mockMvc.perform(
                delete("/api/v1/merchants/" + merchant.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<MerchantResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNull(res.getData());

            Merchant merchantDB = merchantRepository.findById(merchant.getId()).orElse(null);
            assertNull(merchantDB);

        });
    }
}