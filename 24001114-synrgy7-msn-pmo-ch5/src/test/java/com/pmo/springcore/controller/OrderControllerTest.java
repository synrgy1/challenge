package com.pmo.springcore.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.entity.Order;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IMerchantRepository;
import com.pmo.springcore.repository.IOrderRepository;
import com.pmo.springcore.repository.IUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {
    private final MockMvc mockMvc;
    private final IOrderRepository orderRepository;
    private final IUserRepository userRepository;
    private final IMerchantRepository merchantRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    OrderControllerTest(MockMvc mockMvc, IOrderRepository orderRepository, IUserRepository userRepository, IMerchantRepository merchantRepository, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.merchantRepository = merchantRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setUp() {
        orderRepository.deleteAll();
        merchantRepository.deleteAll();
        userRepository.deleteAll();

        User user = new User();
        user.setToken("token");
        user.setRoleId(1);
        user.generateExpiredToken();
        userRepository.save(user);
    }

    @AfterEach
    void tearDown() {
        orderRepository.deleteAll();
        merchantRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void testCreateOrderUnauthorized() throws Exception{
        CreateOrderRequest req = new CreateOrderRequest();
        req.setCompleted(false);
        req.setDestinationAddress("Surabaya");

        mockMvc.perform(
                post("/api/v1/orders/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateOrderWithBlankDestinationAddressField() throws Exception {
        CreateOrderRequest req = new CreateOrderRequest();
        req.setCompleted(false);

        mockMvc.perform(
                post("/api/v1/orders/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateOrderWithBlankCompletedField() throws Exception {
        CreateOrderRequest req = new CreateOrderRequest();
        req.setDestinationAddress("Surabaya");

        mockMvc.perform(
                post("/api/v1/orders/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateOrderwithoutRequest() throws Exception {

        mockMvc.perform(
                post("/api/v1/orders/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isUnsupportedMediaType()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateOrderSuccess() throws Exception {
        CreateOrderRequest req = new CreateOrderRequest();
        req.setCompleted(false);
        req.setDestinationAddress("Surabaya");

        mockMvc.perform(
                post("/api/v1/orders/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());
        });
    }

    @Test
    void testGetOrderByIdUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                get("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());

            Order order1 = orderRepository.findById(order.getId()).orElse(null);
            assertNotNull(order1);
        });
    }

    @Test
    void testGetOrderByIdDifferentOwner() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        User user1 = new User();
        user1.setToken("beda-token");
        user1.setRoleId(1);
        user1.generateExpiredToken();
        userRepository.save(user1);

        mockMvc.perform(
                get("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());

            Order order1 = orderRepository.findById(order.getId()).orElse(null);
            assertNotNull(order1);
        });
    }

    @Test
    void testGetOrderByIdInvalidId() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                get("/api/v1/orders/salah-" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());

            Order order1 = orderRepository.findById(order.getId()).orElse(null);
            assertNotNull(order1);
        });
    }

    @Test
    void testGetOrderByIdSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                get("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            assertFalse(res.getData().getCompleted());
            assertEquals("Surabaya", res.getData().getDestinationAddress());
            assertEquals(user.getId(), res.getData().getUserId());
        });
    }

    @Test
    void testListOrderUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                get("/api/v1/orders/list")
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<Object> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());

            Order order1 = orderRepository.findById(order.getId()).orElse(null);
            assertNotNull(order1);
        });
    }

    @Test
    void testUpdateOrderUnauthorized() throws Exception{
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        UpdateOrderRequest req = new UpdateOrderRequest();
        req.setCompleted(true);

        mockMvc.perform(
                patch("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateOrderDifferentOwner() throws Exception{
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        User user1 = new User();
        user1.setUsername("beda-user");
        user1.setToken("beda-token");
        user1.setRoleId(1);
        user1.generateExpiredToken();
        userRepository.save(user1);

        UpdateOrderRequest req = new UpdateOrderRequest();
        req.setCompleted(true);

        mockMvc.perform(
                patch("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateOrderSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        UpdateOrderRequest req = new UpdateOrderRequest();
        req.setCompleted(true);

        mockMvc.perform(
                patch("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            Order orderDB = orderRepository.findById(order.getId()).orElse(null);
            assertNotNull(orderDB);
            assertTrue(orderDB.getCompleted());
        });
    }

    @Test
    void testDeleteOrderUnauthorized() throws Exception{
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                delete("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteOrderDifferentOwner() throws Exception{
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        User user1 = new User();
        user1.setUsername("beda-user");
        user1.setToken("beda-token");
        user1.setRoleId(1);
        user1.generateExpiredToken();
        userRepository.save(user1);

        mockMvc.perform(
                delete("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteOrderSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Order order = new Order();
        order.setDestinationAddress("Surabaya");
        order.setCompleted(false);
        order.setUser(user);
        orderRepository.save(order);

        mockMvc.perform(
                delete("/api/v1/orders/" + order.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<OrderResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNull(res.getData());

            Order orderDB = orderRepository.findById(order.getId()).orElse(null);
            assertNull(orderDB);
        });
    }
}