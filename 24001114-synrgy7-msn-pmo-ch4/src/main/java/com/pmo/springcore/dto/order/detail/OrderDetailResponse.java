package com.pmo.springcore.dto.order.detail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDetailResponse {
    private UUID id;
    private Long quantity;
    private Long totalPrice;
    private UUID orderId;
    private UUID productId;
}
