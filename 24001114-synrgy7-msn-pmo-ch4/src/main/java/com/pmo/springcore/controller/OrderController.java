package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/api/v1/orders")
@Slf4j
public class OrderController {

    private final IOrderService orderService;

    @Autowired
    public OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(
            path = "/",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> create(User user, @RequestBody CreateOrderRequest request){
        log.info("controller : create order by user-id : " + user.getId());
        OrderResponse orderResponse = orderService.create(user, request);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order created successfully")
                .data(orderResponse)
                .build();

    }

    @GetMapping(
            path = "/{orderId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> getById(User user, @PathVariable String orderId){
        log.info("controller : get order by id by user-id : " + user.getId());
        OrderResponse orderResponse = orderService.getById(user, orderId);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order created successfully")
                .data(orderResponse)
                .build();
    }

    @GetMapping(
            path = "/list",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<OrderResponse>> listOrder(User user, @RequestParam(name = "completed", required = false) Boolean completed, Pageable pageable){
        log.info("controller : get list orders by user-id : " + user.getId());
        Page<OrderResponse> orderResponseList = orderService.listOrder(user, (Objects.nonNull(completed)) ? completed : null, pageable);
        return Response.<Page<OrderResponse>>builder()
                .status("OK")
                .message("here's the order")
                .data(orderResponseList)
                .build();
    }

    @PatchMapping(
            path = "/{orderId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> update(User user, @RequestBody UpdateOrderRequest request, @PathVariable String orderId){
        log.info("controller : update order by user-id : " + user.getId());

        request.setId(orderId);

        OrderResponse orderResponse = orderService.update(user, request);

        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order updated successfully")
                .data(orderResponse)
                .build();
    }

    @DeleteMapping(
            path = "/{orderId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(User user, @PathVariable String orderId){
        log.info("controller : delete order by user-id : " + user.getId());
        orderService.delete(user, orderId);

        return Response.builder()
                .status("OK")
                .message("order deleted successfully")
                .build();
    }
}
