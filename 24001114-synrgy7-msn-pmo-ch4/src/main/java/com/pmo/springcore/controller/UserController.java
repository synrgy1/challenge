package com.pmo.springcore.controller;

import com.pmo.springcore.dto.user.*;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IAuthService;
import com.pmo.springcore.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/users")
@Slf4j
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
    }

    @PostMapping(
            path = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> register(@RequestBody RegisterRequest request) {
        log.info("controller : new register request");
        userService.register(request);
        return Response.builder().status("OK").message("user created successfully").build();
    }

    @GetMapping(
            path = "/current",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<UserResponse> get(User user) {
        log.info("controller : get user detail request by user-id : " + user.getId());
        UserResponse userResponse = userService.get(user);
        return Response.<UserResponse>builder().status("OK").message("user found").data(userResponse).build();
    }

    @PatchMapping(
            path = "/current",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<UserResponse> update(User user, @RequestBody UpdateUserRequest request) {
        log.info("controller : update user request by user-id : " + user.getId());
        UserResponse updateResponse = userService.update(user, request);
        return Response.<UserResponse>builder().status("OK").message("user updated successfully").data(updateResponse).build();
    }

    @DeleteMapping(
            path = "/current",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(User user){
        log.info("controller : delete user account by user-id : " + user.getId());
        userService.delete(user);
        return Response.builder().status("OK").message("user deleted successfully").build();
    }
}
